package com.ambrella.weather

import android.annotation.SuppressLint
import com.ambrella.weather.retrofit.CityApiClient
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.ambrella.weather.Adapters.dailyAdapter
import com.ambrella.weather.Adapters.historyAdapter
import com.ambrella.weather.Adapters.hourlyAdapter
import com.ambrella.weather.databinding.FragmentInfoBinding

import com.ambrella.weather.retrofit.CityDetailApiClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InfoFragment : Fragment() {
    lateinit var binding: FragmentInfoBinding
    var lon: Double = 0.0
    var lat: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInfoBinding.inflate(inflater)
        return binding.root
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title = requireArguments().getString("title1")
        binding.tvLabel.setText(title)


        val call = CityApiClient.apiClient.getWeatherCity(
            title.toString(),
            "metric",
            "ru",
            "a356083d281fd41b8cc084604cfea1ab"
        )
        call
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->
                val lon = it.coord.lon
                val lat = it.coord.lat
                var temp = it.main.temp
                val desc = it.weather[0].description.toString()

                binding.textView.setText(temp.toString())
                binding.textView2.setText(desc)
                Log.d("TAGS", "широта: " + lon + " Долгота: " + lat)
                val call2 = CityDetailApiClient.apiClientDetail.getWeatherCityDetail(
                    lat.toString(),
                    lon.toString(),
                    "minutely,alerts",
                    "metric",
                    "a356083d281fd41b8cc084604cfea1ab"
                )
                call2
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        // val test=it.current?.temp
                        val daily = it.daily
                        binding.rvDaily.adapter = dailyAdapter(daily, R.layout.item_day)
                        val hourly = it.hourly
                        binding.rvHourly.adapter = hourlyAdapter(hourly, R.layout.item_hour)
                    }, {// Логируем ошибку
                            error ->
                        Log.e("TAGS2", error.toString())
                    })

            },
                { error ->
                    // Логируем ошибку
                    Log.e("TAGS1", error.toString())
                    binding.textView.setText("Введенный вами Город не найден")

                })


        binding.downfrac.setOnClickListener {
            transitionFrag(view,R.id.mainFragment)
        }

    }

    private fun transitionFrag(view: View,fragnId:Int) {
        val navController = Navigation.findNavController(view)
        findNavController().navigate(fragnId, null)
        navController.navigate(fragnId)
    }


    companion object {
        @JvmStatic
        fun newInstance() = InfoFragment()
    }
}