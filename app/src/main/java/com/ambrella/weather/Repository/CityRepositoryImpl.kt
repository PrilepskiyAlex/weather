package com.ambrella.weather.Repository

import android.content.Context
import com.ambrella.weather.Model.Room.DaoCity
import com.ambrella.weather.Model.Room.RoomDatabaseCity
import com.ambrella.weather.Model.Room.tableCity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class CityRepositoryImpl(context: Context, private val backgroundDispatcher: CoroutineDispatcher) :
    RepositoryCity {
    private val cityDao: DaoCity

    init {
        val database = RoomDatabaseCity.getInstance(context, backgroundDispatcher)
        cityDao = database!!.daoCity()
    }

    override fun getAllCity(): Flow<List<tableCity>> {
        return cityDao.getCity()
    }

    override suspend fun insert(city: tableCity) {
        withContext(backgroundDispatcher)
        {
            cityDao.insert(city)
        }
    }

    override suspend fun deleteAll() {
        withContext(backgroundDispatcher) {
            cityDao.deleteAll()
        }
    }

    override suspend fun delete(city: tableCity) {
        withContext(backgroundDispatcher) {
            cityDao.delete(city)
        }
    }


}